CREATE TABLE cars
(
    id             SERIAL PRIMARY KEY,
    name           varchar(255) NOT NULL,
    is_started     boolean      NOT NULL DEFAULT FALSE,
    is_locked      boolean      NOT NULL DEFAULT TRUE,
    gasoline_state decimal      NOT NULL,
    coords         point        NOT NULL,
    is_available   boolean      not null default true
);

CREATE TABLE rides
(
    id         uuid PRIMARY KEY,
    car_id     int REFERENCES cars (id) NOT NULL
);

CREATE TABLE rides_logs
(
    id             uuid PRIMARY KEY,
    ride_id        uuid REFERENCES cars (id) NOT NULL,
    coords         point                    NOT NULL,
    gasoline_state decimal                  NOT NULL,
    time           timestamp                NOT NULL
);

INSERT INTO cars
    (name, is_started, is_locked, gasoline_state, coords)
VALUES ('mersedes amg1', false, true, 20, '37.123123, 54.123123'),
       ('mersedes amg1', false, true, 20, '37.456123, 54.568712'),
       ('mersedes amg2', false, true, 20, '37.645123, 54.567234'),
       ('mersedes amg3', false, true, 20, '37.123453, 54.123543'),
       ('mersedes amg3', false, true, 20, '37.123765, 54.436678'),
       ('bmw x5', false, true, 20, '37.678123, 54.789123'),
       ('bmw x5', false, true, 20, '37.567123, 54.789345'),
       ('bmw x5', false, true, 20, '37.345567, 54.345678'),
       ('bmw x3', false, true, 20, '37.678324, 54.456678'),
       ('bmw x3', false, true, 20, '37.456234, 54.678234'),
       ('bmw x7', false, true, 20, '37.456678, 54.123435'),
       ('bmw i7', false, true, 20, '37.563421, 54.678234'),
       ('tyota supra', false, true, 20, '37.234543, 54.456555'),
       ('tyota camry', false, true, 20, '37.734134, 54.845623'),
       ('tyota supra', false, true, 20, '37.6785435, 54.534645'),
       ('tyota camry', false, true, 20, '37.456234, 54.7892345'),
       ('tyota prius', false, true, 20, '37.4562454, 54.768345'),
       ('tyota prius', false, true, 20, '37.745645, 54.256455'),
       ('tyota camry', false, true, 20, '37.987545, 54.855426');



