package ru.tinkoff.carsharing.carservice

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import ru.tinkoff.carsharing.carservice.api.endpoints.{CarEndpoint, RidingEndpoint}
import sttp.tapir.server.akkahttp.AkkaHttpServerInterpreter

object Main {

  def main(args: Array[String]): Unit = {
    implicit val as: ActorSystem = ActorSystem()
    import as.dispatcher

    val carEndpoint = new CarEndpoint
    val ridingEndpoint = new RidingEndpoint

    val allEndpoints =
      carEndpoint.all ::: ridingEndpoint.all

    val routes: Route = AkkaHttpServerInterpreter.toRoute(allEndpoints)
    Http().newServerAt("localhost", 8081)
      .bind(routes)
      .foreach(b => println(s"server started at ${b.localAddress}"))
  }

}
