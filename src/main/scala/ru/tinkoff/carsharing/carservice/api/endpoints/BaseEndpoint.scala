package ru.tinkoff.carsharing.carservice.api.endpoints

import akka.actor.ActorSystem
import ru.tinkoff.carsharing.carservice.db.repositories.{CarRepository, RidingRepository}
import ru.tinkoff.carsharing.carservice.api.exceptions.HttpRequestError
import ru.tinkoff.carsharing.carservice.services.{CarService, RidingService}
import sttp.tapir.{endpoint, _}

import scala.concurrent.Future
import scala.util.{Failure, Success}

trait BaseEndpoint {
  private implicit val as = ActorSystem()
  import as.dispatcher

  protected val baseEndpointV1 =
    endpoint.in("api" / "v1")

  private val carRepository = new CarRepository
  protected val carService = new CarService(carRepository)
  protected val ridingService = new RidingService(carRepository, new RidingRepository)

  def handleErrors[T](f: Future[T]): Future[Either[HttpRequestError, T]] =
    f.transform {
      case Success(v) => Success(Right(v))
      case Failure(e) => Success(Left(HttpRequestError(e.getMessage)))
    }

}
