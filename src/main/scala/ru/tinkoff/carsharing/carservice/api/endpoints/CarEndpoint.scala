package ru.tinkoff.carsharing.carservice.api.endpoints

import akka.actor.ActorSystem
import io.circe.generic.auto._
import ru.tinkoff.carsharing.carservice.db.models.Car
import ru.tinkoff.carsharing.carservice.api.exceptions.HttpRequestError
import ru.tinkoff.carsharing.carservice.api.models.requests.{EndCarRequest, StartCarRequest}
import ru.tinkoff.carsharing.carservice.api.models.responses.AvailableCarResponse
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe._
import sttp.tapir.path

import scala.concurrent.Future

class CarEndpoint extends BaseEndpoint {
  implicit val as = ActorSystem()
  import as.dispatcher

  protected val baseCarEndpoint =
    baseEndpointV1.in("car")

  private val get =
    baseCarEndpoint
      .get
      .in(path[Long])
      .out(jsonBody[Car])
      .errorOut(jsonBody[HttpRequestError])
      .serverLogic[Future] { id: Long =>
        carService.findById(id)
          .unsafeToFuture
          .map {
            case Some(v) => Right(v)
            case None => Left(HttpRequestError("not found"))
          }
      }

  private val getAvailable =
    baseCarEndpoint
      .get
      .in("available")
      .out(jsonBody[AvailableCarResponse])
      .errorOut(jsonBody[HttpRequestError])
      .serverLogic[Future] { _ =>
        carService.findAvailable()
          .unsafeToFuture
          .map { v => Right.apply(AvailableCarResponse(v)) }
          .recover {
            case e =>
              e.printStackTrace()
              Left(HttpRequestError(e.getMessage))
          }
      }

  private val startCarEndPoint =
    baseCarEndpoint
      .post
      .in("start")
      .in(jsonBody[StartCarRequest])
      .out(jsonBody[Car])
      .errorOut(jsonBody[HttpRequestError])
      .serverLogic[Future] { request =>
        carService.start(request.carId)
          .unsafeToFuture
          .map { v => Right.apply(v) }
          .recover {
            case e =>
              e.printStackTrace()
              Left(HttpRequestError(e.getMessage))
          }
      }

  private val endCarEndPoint =
    baseCarEndpoint
      .post
      .in("end")
      .in(jsonBody[EndCarRequest])
      .out(jsonBody[Car])
      .errorOut(jsonBody[HttpRequestError])
      .serverLogic[Future] { request =>
        carService.end(request.carId)
          .unsafeToFuture
          .map { v => Right.apply(v) }
          .recover {
            case e =>
              e.printStackTrace()
              Left(HttpRequestError(e.getMessage))
          }
      }

  val all = List(get, getAvailable, startCarEndPoint, endCarEndPoint)

}
