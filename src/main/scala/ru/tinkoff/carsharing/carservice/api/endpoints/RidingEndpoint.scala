package ru.tinkoff.carsharing.carservice.api.endpoints

import akka.actor.ActorSystem
import io.circe.generic.auto._
import ru.tinkoff.carsharing.carservice.db.models.Car
import ru.tinkoff.carsharing.carservice.api.exceptions.HttpRequestError
import ru.tinkoff.carsharing.carservice.api.models.requests.{EndRidingRequest, StartRidingRequest}
import ru.tinkoff.carsharing.carservice.api.models.responses.{EndRidingResponse, StartRidingResponse}
import sttp.tapir.generic.auto._
import sttp.tapir.json.circe._
import sttp.tapir.path
import sttp.tapir.{endpoint, _}

import scala.concurrent.Future

class RidingEndpoint extends BaseEndpoint{
  implicit val as = ActorSystem()
  import as.dispatcher

  val baseRidingEndpoint = baseEndpointV1
    .in("car" / "ride")

  private val startRidingEndpoint =
    baseRidingEndpoint
      .post
      .in("start")
      .in(jsonBody[StartRidingRequest])
      .out(jsonBody[StartRidingResponse])
      .errorOut(jsonBody[HttpRequestError])
      .serverLogic[Future] { request =>
        ridingService.startRiding(request.carId)
          .unsafeToFuture
          .map{Right.apply}
          .recover {
            case e =>
              e.printStackTrace()
              Left(HttpRequestError(e.getMessage))
          }
      }

  private val endRidingEndpoint =
    baseRidingEndpoint
      .post
      .in("end")
      .in(jsonBody[EndRidingRequest])
      .out(jsonBody[EndRidingResponse])
      .errorOut(jsonBody[HttpRequestError])
      .serverLogic[Future] { request =>
        ridingService.endRiding(request.carRideId)
          .unsafeToFuture
          .map {v => Right.apply(v)}
          .recover {
            case e =>
              e.printStackTrace()
              Left(HttpRequestError(e.getMessage))
          }
      }

  val all = List(startRidingEndpoint, endRidingEndpoint)

}
