package ru.tinkoff.carsharing.carservice.api.exceptions

case class HttpRequestError(message: String) extends Exception