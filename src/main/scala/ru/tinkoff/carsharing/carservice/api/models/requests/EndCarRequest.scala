package ru.tinkoff.carsharing.carservice.api.models.requests

case class EndCarRequest(carId: Long)
