package ru.tinkoff.carsharing.carservice.api.models.requests

import java.util.UUID

case class EndRidingRequest(carRideId: UUID)
