package ru.tinkoff.carsharing.carservice.api.models.requests

case class StartCarRequest(carId: Long)
