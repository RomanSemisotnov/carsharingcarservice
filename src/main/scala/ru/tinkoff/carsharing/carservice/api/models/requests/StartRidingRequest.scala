package ru.tinkoff.carsharing.carservice.api.models.requests

case class StartRidingRequest(carId: Long)
