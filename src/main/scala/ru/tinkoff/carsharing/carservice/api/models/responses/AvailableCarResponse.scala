package ru.tinkoff.carsharing.carservice.api.models.responses

import ru.tinkoff.carsharing.carservice.db.models.Car

case class AvailableCarResponse(availableCars: List[Car])
