package ru.tinkoff.carsharing.carservice.api.models.responses

case class EndRidingResponse(
                              finalCoords: Option[String],
                              carId: Long
                            )
