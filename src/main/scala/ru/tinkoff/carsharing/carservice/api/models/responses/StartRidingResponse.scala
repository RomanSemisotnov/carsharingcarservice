package ru.tinkoff.carsharing.carservice.api.models.responses

import java.util.UUID

case class StartRidingResponse(
                                   carName: String,
                                   rideId: UUID,
                                   currentCoords: String
                                 )
