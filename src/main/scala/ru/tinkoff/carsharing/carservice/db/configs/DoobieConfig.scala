package ru.tinkoff.carsharing.carservice.db.configs

import cats.effect.{Blocker, ContextShift, IO}
import doobie.Transactor
import doobie.util.ExecutionContexts
import doobie.util.transactor.Transactor.Aux

object DoobieConfig {

  private implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContexts.synchronous)

  val conn: Aux[IO, Unit] = Transactor.fromDriverManager[IO](
    "org.postgresql.Driver",
    "jdbc:postgresql://localhost:5432/CarServiceDb",
    "romansemisotnov",
    "",
    Blocker.liftExecutionContext(ExecutionContexts.synchronous) // just for testing
  )


}
