package ru.tinkoff.carsharing.carservice.db.exceptions

case class ModelNotFoundException(model: String, foundBy: AnyRef)
  extends Exception(s"model $model founded by ${foundBy.toString} doesnot exist")
