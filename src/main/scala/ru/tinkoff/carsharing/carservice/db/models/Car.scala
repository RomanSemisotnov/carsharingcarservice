package ru.tinkoff.carsharing.carservice.db.models

case class Car(
                id: Long,
                name: String,
                isStarted: Boolean,
                isLocked: Boolean,
                gasolineState: Double,
                coords: String,
                isAvailable: Boolean
              )
