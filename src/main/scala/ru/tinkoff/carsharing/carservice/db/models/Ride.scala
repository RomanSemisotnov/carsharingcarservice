package ru.tinkoff.carsharing.carservice.db.models

import java.util.UUID


case class Ride(
                 id: UUID,
                 carId: Long
               )
