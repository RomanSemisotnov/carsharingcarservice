package ru.tinkoff.carsharing.carservice.db.models

import java.time.LocalDateTime
import java.util.UUID

case class RideLog(
                    id: UUID,
                    rideId: UUID,
                    coords: String,
                    gasolineState: Double,
                    time: LocalDateTime
                  )
