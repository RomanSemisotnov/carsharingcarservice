package ru.tinkoff.carsharing.carservice.db.repositories

import doobie.ConnectionIO
import doobie.implicits._
import ru.tinkoff.carsharing.carservice.db.models.Car

class CarRepository extends Repository {

  def findById(carId: Long): ConnectionIO[Option[Car]] =
    sql"select id, name, is_started, is_locked, gasoline_state, coords, is_available from cars where id = $carId"
      .query[Car]
      .option

  def findAvailable(): ConnectionIO[List[Car]] =
    (fr"select id, name, is_started, is_locked, gasoline_state, coords, is_available " ++
      fr"from cars where is_available = true")
      .query[Car]
      .to[List]

  def setAvailableCar(carId: Long, isAvailable: Boolean): ConnectionIO[Int] =
    sql"update cars set is_available = $isAvailable where id = $carId"
      .update
      .run

  def start(carId: Long) =
    sql"update cars set is_started = true, is_locked = false where id = $carId"
      .update
      .run

  def end(carId: Long) =
    sql"update cars set is_started = false, is_locked = true where id = $carId"
      .update
      .run

}
