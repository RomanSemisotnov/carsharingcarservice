package ru.tinkoff.carsharing.carservice.db.repositories

import doobie.implicits._

trait Repository {

  def getLastVal() =
    sql"select lastval()".query[Long].unique

}
