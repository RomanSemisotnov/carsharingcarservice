package ru.tinkoff.carsharing.carservice.db.repositories

import doobie.ConnectionIO
import doobie.implicits._
import ru.tinkoff.carsharing.carservice.db.models.{Ride, RideLog}
import doobie.postgres._
import doobie.postgres.implicits._

import java.util.UUID

class RidingRepository extends Repository {

  def startRiding(carId: Long): ConnectionIO[Ride] =
    sql"INSERT INTO rides (id, car_id) VALUES(${UUID.randomUUID}, $carId);"
      .update
      .withUniqueGeneratedKeys[Ride]("id", "car_id")

  def getById(id: UUID): ConnectionIO[Option[Ride]] =
    sql"select id, car_id from rides where id = $id::uuid"
      .query[Ride]
      .option

  def getLastLogsByRideId(rideId: UUID): ConnectionIO[Option[RideLog]] =
    sql"select id, ride_id, coords, gasoline_state, time from rides_logs where ride_id = $rideId::uuid order by time desc limit 1"
      .query[RideLog]
      .option

}
