package ru.tinkoff.carsharing.carservice.services

import cats.effect.IO
import ru.tinkoff.carsharing.carservice.db.models.Car
import ru.tinkoff.carsharing.carservice.db.repositories.CarRepository
import doobie.implicits._
import ru.tinkoff.carsharing.carservice.db.exceptions.ModelNotFoundException

class CarService(carRepository: CarRepository) {

  import ru.tinkoff.carsharing.carservice.db.configs.DoobieConfig.conn

  def findById(id: Long) =
    carRepository.findById(id).transact(conn)

  def findAvailable(): IO[List[Car]] =
    carRepository.findAvailable()
      .transact(conn)
      .map { cars => cars.sortBy(_.id) }

  def start(carId: Long) =
    (for {
      _ <- carRepository.start(carId)
      carOpt <- carRepository.findById(carId)
      car = carOpt.getOrElse(throw ModelNotFoundException("Car", carId.toString))
    } yield car)
      .transact(conn)

  def end(carId: Long) =
    (for {
      _ <- carRepository.end(carId)
      carOpt <- carRepository.findById(carId)
      car = carOpt.getOrElse(throw ModelNotFoundException("Car", carId.toString))
    } yield car)
      .transact(conn)

}
