package ru.tinkoff.carsharing.carservice.services

import cats.effect.IO
import ru.tinkoff.carsharing.carservice.db.exceptions.ModelNotFoundException
import ru.tinkoff.carsharing.carservice.db.repositories.{CarRepository, RidingRepository}
import doobie.implicits._
import ru.tinkoff.carsharing.carservice.api.models.responses.{EndRidingResponse, StartRidingResponse}

import java.util.UUID

class RidingService(carRepository: CarRepository, ridingRepository: RidingRepository) {

  import ru.tinkoff.carsharing.carservice.db.configs.DoobieConfig.conn

  def startRiding(carId: Long) =
    (for {
      carOpt <- carRepository.findById(carId)
      car = carOpt.getOrElse(throw ModelNotFoundException("Car", carId.toString))
      _ <- carRepository.setAvailableCar(carId, isAvailable = false)
      newRiding <- ridingRepository.startRiding(carId)
        .map { riding => StartRidingResponse(car.name, riding.id, car.coords) }
    } yield newRiding)
      .transact(conn)

  def endRiding(rideId: UUID): IO[EndRidingResponse] =
    (for {
      rideOpt <- ridingRepository.getById(rideId)
      ride = rideOpt.getOrElse(throw ModelNotFoundException("Ride", rideId))
      lastRideLogOpt <- ridingRepository.getLastLogsByRideId(ride.id)
      lastRideCoorLog = lastRideLogOpt.map(_.coords)
      _ <- carRepository.setAvailableCar(ride.carId, isAvailable = true)
    } yield (lastRideCoorLog, ride.carId))
      .transact(conn)
      .map{ case (log, carId) => EndRidingResponse(log, carId)}


}
